Ivan Pompa-García
============

![&#32;](images/yo2_sqr.png){width=15%}

> [Github](https://github.com/ivanjpg) • [Gitlab](https://gitlab.com/ivanjpg) • [Linkedin](https://www.linkedin.com/in/ivan-jpg/) • [+52 55 2840 3943](tel:+525528403943)

> [ivanjpg@ekbt.nl](mailto:ivanjpg@ekbt.nl) • [ekbt.nl](https://ekbt.nl/)

----

> Apasionado del conocimiento y solucionador de problemas nato.
> Si bien mis habilidades obtenidas de manera autodidacta
> resaltan mis capacidades para aprender y resolver cualquier tipo de problema, mi formación
> científica muestra mi suficiencia para asimilar y aplicar la instrucción académica formal.
> El pensamiento analítico, las soluciones prácticas y la eficiencia, han estado siempre
> presentes en mi proceso de trabajo.

----

Experiencia
----------

> · Senior Software Engineer. Nubank. · Actividad principal: Desarrollo backend en Clojure y bases de datos en Datomic.· Actividades recurrentes: Codificación de scripts de automatización de tareas en diversos lenguajes (Python, Bash, Clojure, ...). Interacción y administración con servicios en la nube (AWS / GCP). Diseño e implementación de arquitectura hexagonal (Diplomat). Manejo avanzado de BigQuery. Desarrollo móvil con Flutter y herramientas internas.

> · Team Lead: Backend (Senior). NECSUS. · Actividad principal: Desarrollo backend en Java. · Actividades recurrentes: Codificación de scripts para CI/CD en Jenkins y sus configuraciones. Administración de AWS (EC2, Buckets). Administración y despliegue de contenedores Docker. Coaching y Consultoría para el equipo de TI (frontend y backend): Juniors, Semi-seniors y Seniors. Respuesta general a incidentes de todo el Stack Tecnológico de la empresa.

Experiencia como Freelance
----------
> Consultor TI • Desarrollador Web • Desarrollador Móvil

> Administrador de sistemas • Profesor / Instructor • Soporte Técnico

> -> [Lista de experiencia en extenso](#experiencia-en-extenso) <-

Educación
---------

2023-2024
:   **Diplomado en Ciencia de Datos**, Universidad Nacional Autónoma de México, Online

2022 (Agosto - Noviembre)
:   **Sistemas embebidos con cómputo en la nube**, Facultad de Ciencias, UNAM (Online)

2021-2025 (esperado)
:   **Doctorado, Física**; UAM-Iztapalapa (México)

    *Tesis: Difusión confinada y escape de partículas Brownianas: estudio analítico y numérico*

2019-2021
:   **Maestría en Ciencias, Física**; UAM-Iztapalapa (México)

    *Tesis: Difusión en canales bajo un potencial gravitatorio: reducción efectiva a una dimensión*

2013-2018
:   **Licenciatura, Física**; UAM-Iztapalapa (México)
    
    *Tesis: Difusión confinada*

Idiomas
--------------------

> Español [nativo] • Inglés [CEFR: B2, TOEFL ITP: 573]

Habilidades técnicas
--------------------

Machine Learning
:   Scikit-Learn, Pandas, Numpy, Matplotlib, Plotly, Dash, XGBoost.

Cloud
:   **GCP**: Apigee, Storage, Functions [Node \& Python], Scheduler, Run, Compute Engine, S3, Kubernetes Engine. **AWS**: EC2, EKS, EventBridge, Fargate, ApiGateway, Lambda [Python].

DevOps
:   Jenkins, Git, Docker, Selenium, SonarQube.

SOs
:   MacOSX, GNU/Linux, \*BSD, Windows.

Bases de datos
:   MySQL, PostgreSQL, MariaDB, SQLite, MongoDB, Oracle, MS SQLServer.

Lenguajes de programación
:   Ruby, Dart, Python, PHP, C, Pascal, FORTRAN, C\#, Java, Javascript, R, Bash, Julia, Groovy.

Desarrollo móvil
:   Flutter, Titanium Appcelerator, Android SDK (native), iOS (native) (Swift), Apache Cordova (Phonegap).

Web
:   HTML, CSS, SASS, Javascript, JQuery, Bootstrap, RubyOnRails, CodeIgniter, ReactJS.

Otros
:   LaTeX, Mathematica.

Publicaciones científicas
--------------------

- Pompa-García, I., Castilla, R., Metzler, R., & Dagdug, L. (2022). First-passage times in conical varying-width channels biased by a transverse gravitational force: Comparison of analytical and numerical results. [doi:10.1103/PhysRevE.106.064137](https://doi.org/10.1103/PhysRevE.106.064137)

- Pompa-García, I., & Dagdug, L. (2021). Two-dimensional diffusion biased by a transverse gravitational force in an asymmetric channel: Reduction to an effective one-dimensional description. 104(4), 044118. [doi:10.1103/PhysRevE.104.044118](https://doi.org/10.1103/PhysRevE.104.044118)


Experiencia (en extenso)
----------

**Site2Kndl**

- *Desarrollador*.
- *Proyecto personal*.
- Desarrollo de una aplicación para eliminar el contenido irrelevante de un sitio web y enviarlo al dispositivo Kindle a través del servicio de Amazon.
    - Javascript, PHP, Pandoc.

> Noviembre, 2022 -- Actual.

---

**Nacionalidad Mexicana**

- *Desarrollador Móvil / Arquitecto*.
- *Startup Independiente*.
- Desarrollo para iOS, Android y Web.
    - Flutter, Firebase (Auth, Storage, Database), Bash (automatización de compilación y despliegue).

> Septiembre, 2022 -- Actual.

---

**IT Specialized Instructor**

- *IT Instructor / Professor*.
- *Freelance / Tec Gurus*.
- Impartición de cursos especializados de TI a profesionales (ingenieros, informáticos, desarrolladores, ...).
- Cursos impartidos: iOS (Swift), Oracle, MS SQLServer, Selenium, Desarrollo Web, Titanium Appcelerator, Phonegap, NodeJS, Python, .Net, Java, Ciencia de Datos con Python, Flutter.

> Junio, 2012 -- Actual.

---

**Experiencia específica en Google Cloud Platform (GCP)**

- **Cloud Administrator & Developer**
- **-- Empresa Confidencial --**
- Apigee
	- Definición de APIs diferenciadas por ambientes de despliegue (dev, qa, prod).
	- Configuración de rutas de acceso.
	- Protección contra uso indebido con políticas Spike Arrest.
	- Implementación de políticas de transformación (JSON-XML) y decodificación (JWT).

- Cloud Functions [en Python y Node]
	- Inserción de marcas de agua automáticas como respuesta al almacenamiento de archivos PDF y PNG en un bucket de Cloud Storage.
	- Automatización de encendido y apagado de instancias de Google Cloud Compute Engine.

- Cloud Scheduler
	- Programación de automatizaciones de tareas basadas en eventos de tiempo.

- Cloud Run
	- Orquestación básica de despliegue para aplicaciones «dockerizadas» a un sistema central.

- Compute Engine
	- Despliegue y mantenimiento de instancias de cómputo en la nube.
	- Despliegue de aplicaciones «dockerizadas» en las instancias CE.

- Kubernetes Engine
	- Uso básico y despliegues de prueba como parte de una perspectiva futura.

> Marzo, 2020 -- Octubre 2022.

---

**NativeScreenshot**

- *Flutter Desarrollador*.
- *Proyecto personal*
- Plugin de Flutter para tomar capturas de pantalla de manera nativa.

> Enero, 2020 -- Abril, 2021.

---

**Mis Guías**

- *Desarrollador Móvil*.
- *Startup Independiente*.
- Desarrollo para iOS, Android y Web.
    - Flutter, Firebase (Auth, Storage, Database), Bash (automatización de compilación y despliegue).

> Octubre, 2020 -- Actual.

---

**AppArtista**

- *Desarrollador Móvil*.
- *Startup Independiente*.
- Desarrollo para iOS, Android y Web.
    - Flutter, Firebase (Auth, Storage, Database), Bash (automatización de compilación y despliegue).

> Noviembre, 2019 -- Actual.

---

**LifeLike**

- *Desarrollador Móvil / Arquitecto*.
- *Startup Independiente*.
- Desarrollo para iOS.
    - Swift, CoreData & Google Places API.

> Junio, 2018 -- Julio, 2019.

---

**Ayudante de Investigador Nacional**

- *Ayudante de Investigador Nacional.
- *CONACyT, México*.
- Ayudante en labores de docencia e investigación.
    - Administración de servidores (GNU/Linux, Wordpress, Nextcloud).
    - Soporte técnico (Mac, Linux, Windows).
    - Configuración y mantenimiento de servidores de cálculo (GROMACS, Fortran, Mathematica).
    - Investigación en dinámica de partículas Brownianas (sistemas confinados), teoría y simulaciones.

> 2016 -- 2019.

---

**¿Cómo ves?**

- *Desarrollador Web / Arquitecto*.
- *DGDC, UNAM*.
- Desarrollo Web.
    - PHP, CodeIgniter, MySQL, JQuery, CSS, Bootstrap.
    - Configuración de servidor Nginx con vHosts.
- Trabajo en equipo con 2 diseñadores gráficos.

> Agosto, 2016 -- Abril, 2019.

---

**Assistant Professor**

- *Assistant Professor*.
- *UAM-Iztapalapa, México*.
- Impartición de clases y asesorías a estudiantes de la división de Ciencias Básicas e Ingenierías.
    - Cursos: Mecánica Elemental I (7 cursos), Electricidad y Magnetismo Elemental I (2 cursos), Física Moderna I (2 cursos), Mecánica Elemental II (1 curso), Física Computacional (1 curso), Variable Compleja (1 curso), Teoría Electromagnética I (1 curso), Temas Selectos de Física (1 curso), Fluidos y Calor (1 curso).
- Apoyo a tareas de docencia.

> Abril, 2015 -- Marzo, 2018.

---

**LavaTickets**

- *Desarrollador Móvil / Backend*.
- *Negocio Independiente*.
- Desarrollo backend del WebService en PHP con ActiveRecord y SQLite como base de datos.
- Impresión en dispositivo térmico a través de Raspberry PI utilizando ESCPos-PHP.
- Proceso de imágenes para impresora térmica en C con la biblioteca ImageMagick.
- Desarrollo de la aplicación móvil para tablets en Apache Cordova (Phonegap).

> Agosto, 2015 -- Septiembre, 2015.

---

**Helpdesk System (tickets)**

- *Desarrollador Web / Arquitecto*.
- *DGDC, UNAM*.
- Desarrollo Web.
    - RubyOnRails, MySQL, Javascript, CoffeeScript, SASS.
- Configuración y administración de servidor desde cero.
    - GNU/Linux Debian, Apache, MySQL, PHP, SSH.

> Junio, 2015 -- Septiembre, 2015.

---

**Banco de imágenes**

- *Backend Developer*.
- *-- Empresa Confidencial --*.
- Evolución planeada del backend de una aplicación escrita (por mi) en RubyOnRails.
- Procesado de imágenes cargadas en la web usando el API Java 2D para la marca de agua.

> Marzo, 2014 -- Septiembre, 2015.

---

**SIROM / SIAP**

- *Consultor TI / Administrador de sistemas*.
- *DGDG, UNAM*.
- Consultor TI externo para la implementación de un sistema de administración de proyectos (*sirom.fr*).
- Revisión de código (PHP, Javascript) y testing manual del sistema, reporte de bugs.
- Configuración y administración del servidor de instalación final.
    - GNU/Linux Debian, Apache, MySQL, PHP.

> Marzo, 2014 -- Febrero, 2015.

---

**Escuela Preparatoria particular**

- *Backend Developer*.
- *-- Empresa Confidencial --*.
- Proceso de datos provenientes de una estación meteorológica (WMR200).
- Los datos eran guardados en una base PostgreSQL.
- Se presentaban a través de la web usando una aplicación MVC escrita con Spring 3.x.

> Diciembre, 2013 -- Mayo, 2014.

---

**Cheapbook**

- *Desarrollador Móvil / Backend*.
- *Proyecto personal*.
- Aplicación para encontrar el precio más bajo de un libro dado en librerías locales (México.)
- Desarrollo backend del WebService en Ruby (CGI) con mini sistema de caché en SQLite.
- Desarrollo de la aplicación móvil en Titanium Appcelerator+Alloy.

> Noviembre, 2012 -- Junio, 2014.

---

**Photodamnsports.com**

- *Desarrollador Web / Arquitecto*.
- *Negocio Independiente*.
- Desarrollo Web.
    - RubyOnRails, MySQL.
- Desarrollo de sistema de escritorio de proceso de imágenes en lote (MacOSX).
    - MacOSX, Ruby, ImageMagick.
- Configuración y administración de Sharedhosting.
- Trabajo en equipo con 1 diseñador gráfico.

> Agosto, 2013 -- Abril, 2014.

---

**Ciencia UNAM**.

- *Desarrollador Web / Arquitecto / Consultor TI*.
- *DGDC, UNAM*.
- Desarrollo Web (propuesta de reemplazo de sitio web).
    - RubyOnRails, MySQL.
- Trabajo en equipo con 1 diseñador gráfico.

> Marzo, 2013 -- Junio, 2013.

---

**Varios paquetes AUR (Archlinux)**

- *Mantenedor de paquetes AUR*.
- *Online*
- `bamf2`, `dkms-alx`, `guake-fontsize-stable`, `pidgin-libnotify-privacy`,
- `psmouse-alps-driver`, `textadept-latest-stable`.

> Junio, 2012 -- Diciembre, 2012

---

**Bodega Mesones**

- *Desarrollador Web / Administrador de sistemas*.
- *Negocio Independiente*.
- Integración de la identidad gráfica con una tienda en linea (Prestashop).
- Carga en lote automatizada de productos sobre Prestashop.
- Administración de SharedHosting y correo electrónico.
- Trabajo en equipo con 1 diseñador gráfico.

> Febrero, 2012 -- Diciembre, 2013.

---

**Visit Reservation System**

- *Desarrollador Web / Arquitecto*.
- *DGDC, UNAM*.
- Desarrollo Web con generación de PDF.
    - PHP, CodeIgniter, JQuery, CSS.

> Marzo, 2011 -- Agosto, 2012.

---

**Agenda Ciudadana**

- *Desarrollador Web / Arquitecto / Consultor TI*.
- *Academia Mexicana de Ciencias*.
- Consultor de TI.
- Desarrollo Web (sitio web y sistema de votacion).
    - PHP, CodeIgniter, MySQL, JQuery, CSS, Ruby.
    - Configuración y administración de SharedHosting (Hostmonster).
    - Scripts (Ruby) de sincronización de votos consumiendo API remota (*empirika.org*).
    - Scripts (Ruby) de monitoreo y reporte de votos.

> Octubre, 2012 -- Enero, 2013.

---

**Ciencia UNAM**

- *Desarrollador Web / Arquitecto*.
- *DGDC, UNAM*.
- Desarrollo Web.
    - PHP, CodeIgniter, MySQL, JQuery, CSS.
    - Configuración de servidor Nginx con vHosts.
    - Configuración de servidor de caché (Varnish).
    - Configuración de servidor de base de datos (MySQL).
    - Diseño de topología interna de red para servidores.
- Instalación y administración de Wordpress multisite para servicio de blogs.
- Responsable técnico a cargo de 3 estudiantes de servicio social.
- Trabajo en equipo con 2 diseñadores gráficos.

> Marzo, 2010 -- Julio, 2012.

---

**Tienda de conveniencia local**

- *Backend Developer*.
- *-- Empresa Confidencial --*.
- Sistema de inventarios con herramienta de reporte Excel (.csv).
- Frontend escrito en Delphi.
- El backend se conformó con un webservice escrito con Spring 2.x.
- Para la base de datos se usó Hibernate y MySQL.

> Agosto, 2009 -- Febrero, 2010.

---

**Curso de Unix**

- *Profesor / Instructor*.
- *UPIICSA Tepepan, IPN*.
- Impartido como capacitación a personal del ISSSTE (proveedor de seguridad social del estado).
- Elaboración de un manual básico de comandos de Bash.

> 2009 (2 meses).

---

**DGDC, UNAM**

- *Soporte técnico*.
- Mantenimiento y actualización de equipo (PC & Mac).

> 2007

---

**Ferretería Local**

- *Developer*.
- *-- Empresa Confidencial --*.
- Sistema de Comprador - Vendedor - Gastos.
- Construido usando JSP (Java).

> Julio, 2006 -- Marzo, 2008.

---

----

> <ivanjpg@ekbt.nl> • +52 55 2840 3943 • Ciudad de México, México
