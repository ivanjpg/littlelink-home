Ivan Pompa-García
============

![&#32;](./images/yo2_sqr.png){width=15%}

> [Github](https://github.com/ivanjpg) • [Gitlab](https://gitlab.com/ivanjpg) • [Linkedin](https://www.linkedin.com/in/ivan-jpg/) • [+52 55 2840 3943](tel:+525528403943)

> [ivanjpg@ekbt.nl](mailto:ivanjpg@ekbt.nl) • [ekbt.nl](https://ekbt.nl/)


----

> Passionate about Knowledge \& Natural Problem-Solver.
> As my self-taught skills hold for my independent
> capacities to learn and solve any kind of problem, my scientific academic degrees show my 
> ability to assimilate and apply academic formal instruction. Analytical thinking,
> practical solutions and efficiency has been always present in my work process.

----

Experience
----------

> · Senior Software Engineer. Nubank. · Main activity: Backend development using Clojure with Datomic as database. · Recurrent activities: Script development to automatize tasks using several programming languages (Python, Bash, Clojure, ...). Administration and interaction with Cloud Services (AWS / GCP). Design and implementation using hexagonal architecture (Diplomat). BigQuery advanced use and query development. Mobile development with Flutter and internal tools.

> · Team Lead: Backend (Senior). NECSUS. · Main activity: Java backend development. · Recurrent activities: Jenkins configuration and CI scripts coding. AWS administration (EC2, Buckets). Docker containers administration and deployment management. Coach and Consultant for IT team (frontend and backend): Juniors, Semi-seniors and Seniors. General incident response of the whole Tech Stack.

Freelance Experience
----------

> IT Consultant • Web Developer • Mobile Developer

> Systems Administrator • Professor / Instructor • Tech Support

> -> [Extended list of experience](#experience-extended) <-

Education
---------

2023-2024
:   **Data Science Qualification Program**, Universidad Nacional Autónoma de México, Online

2022 (August - November)
:   **Embedded Systems with Cloud Computing**, Facultad de Ciencias, UNAM (Online)

2021-2025 (expected)
:   **PhD, Physics**; UAM-Iztapalapa (México)

    *Thesis (in spanish): Confined diffusion and Brownian particle exiting: analytical & numerical study*

2019-2021
:   **MSc, Physics**; UAM-Iztapalapa (México)

    *Thesis (in spanish): Confined diffusion in channels under a gravitational-like potential: effective one-dimensional reduction*

2013-2018
:   **BSc, Physics**; UAM-Iztapalapa (México)
    
    *Thesis (in spanish): Confined diffusion*

Languages
--------------------

> Spanish [native speaker] • English [CEFR: B2, TOEFL ITP: 573].

Technical Skills
--------------------

Machine Learning
:   Scikit-Learn, Pandas, Numpy, Matplotlib, Plotly, Dash, XGBoost.

Cloud
:   **GCP**: Apigee, Storage, Functions [Node \& Python], Scheduler, Run, Compute Engine, S3, Kubernetes Engine. **AWS**: EC2, EKS, EventBridge, Fargate, ApiGateway, Lambda [Python].

DevOps
:   Jenkins, Git, Docker, Selenium, SonarQube.

OSes
:   MacOSX, GNU/Linux, \*BSD, Windows.

Databases
:   MySQL, PostgreSQL, MariaDB, SQLite, MongoDB, Oracle, MSSQLServer.

Programming languages
:   Ruby, Dart, Python, PHP, C, Pascal, FORTRAN, C\#, Java, Javascript, R, Bash, Julia, Groovy.

Mobile development
:   Flutter, Titanium Appcelerator, Android SDK (native), iOS (native) (Swift), Apache Cordova (Phonegap).

Web
:   HTML, CSS, SASS, Javascript, JQuery, Bootstrap, RubyOnRails, CodeIgniter, ReactJS.

Other
:   LaTeX, Mathematica.

Scientific Publications
--------------------
- Dagdug, L., Peña, J., Pompa-García, I. (2024). Diffusion Under Confinement: A Journey Through Counterintuition. [ISBN: 978-3031464744](https://isbnsearch.org/isbn/9783031464744)

- Pompa-García, I., Castilla, R., Metzler, R., & Dagdug, L. (2023). Two-dimensional diffusion biased by a transverse gravitational force in an asymmetric channel. [doi:10.1063/5.0134092](https://doi.org/10.1063/5.0134092)

- Pompa-García, I., Castilla, R., Metzler, R., & Dagdug, L. (2022). First-passage times in conical varying-width channels biased by a transverse gravitational force: Comparison of analytical and numerical results. [doi:10.1103/PhysRevE.106.064137](https://doi.org/10.1103/PhysRevE.106.064137)

- Pompa-García, I., & Dagdug, L. (2021). Two-dimensional diffusion biased by a transverse gravitational force in an asymmetric channel: Reduction to an effective one-dimensional description. 104(4), 044118. [doi:10.1103/PhysRevE.104.044118](https://doi.org/10.1103/PhysRevE.104.044118)


Experience (extended)
----------

**Site2Kndl**

- *Developer*.
- *Personal Project*.
- Application development to remove the irrelevant content of a website & send the result to a Kindle device through Amazon service.
    - Javascript, PHP, Pandoc.

> November, 2022 -- Current.

---

**Nacionalidad Mexicana**

- *Mobile Developer / Architect*.
- *Independent Startup*.
- Development for iOS, Android y Web.
    - Flutter, Firebase (Auth, Storage, Database), Bash (build & deployment automation).

> September, 2022 -- Current.

---

**IT Specialized Instructor**

- *IT Instructor / Professor*.
- *Freelance / Tec Gurus*.
- Teaching classes (courses) to IT professionals (engineers, developers, ...).
- Taught courses: iOS (Swift), Oracle, MS SQLServer, Selenium, Web Development, Titanium Appcelerator, Phonegap, NodeJS, Python, .Net, Java, Data Science with Python, Flutter.

> June, 2012 -- Current.

---

**Specifics of Google Cloud Platform (GCP)**

- **Cloud Administrator & Developer**
- **-- Confidential Company --**
- Apigee
	- APIs definition and differentiation by deployment environments (dev, qa, prod).
	- Routing configuration.
  - Spike Arrest protection policies implementation.
  - Transformation policies configuration (JSON-XML).
  - Decoding policies configuration (JWT).

- Cloud Functions [with Python & Node]
  - Automatic watermarking triggered by PDF and PNG uploads to Cloud Storage.
  - Start & stop automatization for Google Cloud Compute Engine instances.

- Cloud Scheduler
  - Cron-based automatizations.

- Cloud Run
  - Basic orquestation and deployment of «dockerized» applications.

- Compute Engine
  - Mantainance and deployment of cloud computing instances.
  - Deployment of «dockerized» applications into the Compute Engine.

- Kubernetes Engine
  - Basic use and testing deployments as a planing for a future project.

> March, 2020 -- October 2022.

---

**NativeScreenshot**

- *Flutter Developer*.
- *Personal Project*
- Flutter plugin to take screenshots in a *native* way.

> January, 2020 -- April, 2021.

---

**Mis Guías**

- *Mobile Developer*.
- *Independent Startup*.
- Development for iOS, Android y Web.
    - Flutter, Firebase (Auth, Storage, Database), Bash (build & deployment automation).

> October, 2020 -- Current.

---

**AppArtista**

- *Mobile Developer*.
- *Independent Startup*.
- Development for iOS, Android y Web.
    - Flutter, Firebase (Auth, Storage, Database), Bash (build & deployment automation).

> November, 2019 -- Current.

---

**LifeLike**

- *Mobile Developer / Architect*.
- *Independent Startup*.
- Development for iOS.
    - Swift, CoreData & Google Places API.

> June, 2018 -- July, 2019.

---

**National Researcher Assistant, Level III**

- *National Researcher Assistant*.
- *CONACyT, México*.
- Teaching & Research assistance.
    - Server administration (GNU/Linux, Wordpress, Nextcloud).
    - Tech support (Mac, Linux, Windows).
    - Configuration & mainteinance of calculus servers (GROMACS, Fortran, Mathematica).
    - Research in the field of Brownian dynamics (confined systems), theory & simulations.

> 2016 -- 2019.

---

**¿Cómo ves?**

- *Web Developer / Architect*.
- *DGDC, UNAM*.
- Web Development.
    - PHP, CodeIgniter, MySQL, JQuery, CSS, Bootstrap.
    - Server configuration with Nginx & vHosts.
- Team work with 2 graphic designers.

> August, 2016 -- April, 2019.

---

**Assistant Professor**

- *Assistant Professor*.
- *UAM-Iztapalapa, México*.
- Teaching classes to Engineering & Basic Sciences Division students.
    - Taught courses: Elementary Mechanics I (7 courses), Elementary Electricity and Magnetism I (2 courses), Modern Physics I (2 courses), Elementary Mechanics II (1 course), Computational Physics (1 course), Complex Variable (1 course), Electromagnetic Theory I (1 course), Physics Selected Topics (1 course), Heat and Fluids (1 course).
- Teaching  assistance.

> April, 2015 -- March, 2018.

---

**LavaTickets**

- *Mobile Developer / Backend*.
- *Independent Business*.
- PHP WebService backend development using ActiveRecord & SQLite as database.
- Thermal printing through a Raspberry PI with ESCPos-PHP.
- Thermal printing image preprocessing in C using ImageMagick library.
- Mobile application development for tablets with Apache Cordova (Phonegap).

> August, 2015 -- September, 2015.

---

**Helpdesk System (tickets)**

- *Web Developer / Architect*.
- *DGDC, UNAM*.
- Web Development.
    - RubyOnRails, MySQL, Javascript, CoffeeScript, SASS.
- Server Configuration & Administration from scratch.
    - GNU/Linux Debian, Apache, MySQL, PHP, SSH.

> June, 2015 -- September, 2015.

---

**Image Stock**

- *Backend Developer*.
- *-- Confidential Company --*.
- Planned evolution of the backend written (by me) in RubyOnRails.
- Processing of the uploaded images with Java 2D API for watermarking.

> March, 2014 -- September, 2015.

---

**SIROM / SIAP**

- *IT Consultant / System Administrator*.
- *DGDG, UNAM*.
- External IT consultant for a project management system implementation (*sirom.fr*).
- Code review (PHP, Javascript) & manual testing, bug reporting.
- Server Configuration & Administration for the deployed system.
    - GNU/Linux Debian, Apache, MySQL, PHP.

> March, 2014 -- February, 2015.

---

**Particular High School**

- *Backend Developer*.
- *-- Confidential Company --*.
- Processing data from a weather station (WMR200).
- Saved in a PostgreSQL database.
- Presented over the web through a Spring 3.x application with MVC.

> December, 2013 -- May, 2014.

---

**Cheapbook**

- *Mobile Developer / Backend*.
- *Personal Project*.
- Application to get the lowest price for a book, retrieved from local (mexican) libraries websites.
- Ruby (CGI) WebService backend development with mini-cache SQLite system.
- Mobile application development in Titanum Appcelerator + Alloy.

> November, 2012 -- June, 2014.

---

**Photodamnsports.com**

- *Web Developer / Architect*.
- *Independent*.
- Web Development.
    - RubyOnRails, MySQL.
- Desktop application for image processing.
    - MacOSX, Ruby, ImageMagick.
- Shared hosting configuration & administration.
- Team work with 1 graphic designer.

> August, 2013 -- April, 2014.

---

**Ciencia UNAM**.

- *Web Developer / Architect / IT Consultant*.
- *DGDC, UNAM*.
- Web Development (replacement system proposal).
    - RubyOnRails, MySQL.
- Team work with 1 graphic designer.

> March, 2013 -- June, 2013.

---

**Several AUR (Archlinux) packages**

- *AUR Package Mantainer*.
- *Online*
- `bamf2`, `dkms-alx`, `guake-fontsize-stable`, `pidgin-libnotify-privacy`,
- `psmouse-alps-driver`, `textadept-latest-stable`.

> June, 2012 -- December, 2012

---

**Bodega Mesones**

- *Web Developer / System Administrator*.
- *Independent Business*.
- Graphic identity integration with online store system (Prestashop).
- Automated batch product load.
- Shared hosting configuration, administration & email administration (*XpressHosting*).
- Team work with 1 graphic designer.

> February, 2012 -- December, 2013.

---

**Visit Reservation System**

- *Web Developer / Architect*.
- *DGDC, UNAM*.
- Web Development with PDF generation.
    - PHP, CodeIgniter, JQuery, CSS.

> March, 2011 -- August, 2012.

---

**Agenda Ciudadana**

- *Web Developer / Architect / IT Consultant*.
- *Academia Mexicana de Ciencias*.
- IT Consultant.
- Web Development (website & voting system).
    - PHP, CodeIgniter, MySQL, JQuery, CSS, Ruby.
    - Shared hosting configuration & administration. (*Hostmonster*).
    - Ruby scripts for remote voting API sync (*empirika.org*).
    - Ruby script for voting reporting & monitoring.

> October, 2012 -- January, 2013.

---

**Ciencia UNAM**

- *Web Developer / Architect*.
- *DGDC, UNAM*.
- Web Development.
    - PHP, CodeIgniter, MySQL, JQuery, CSS.
    - Nginx server configuration with vHosts.
    - Cache server configuration (Varnish).
    - Database server configuration (MySQL).
    - Internal network topology design for servers.
- Wordpress multisite Installation & Administration for bloggin service.
- Technical management of 3 students.
- Team work with 2 graphic designers.

> March, 2010 -- July, 2012.

---

**Local Retail Store**

- *Backend Developer*.
- *-- Confidential Company --*.
- Inventory System with an Excel (.csv) reporting tool.
- The frontend was written in Delphi.
- The backend was a webservice written using Spring 2.X.
- For the database, Hibernate and MySQL was used.

> August, 2009 -- February, 2010.

---

**Unix Course**

- *Professor / Instructor*.
- *UPIICSA Tepepan, IPN*.
- Taught course to ISSSTE (goverment health-care provider).
- Bash command line basic manual preparation.

> 2009 (2 months).

---

**DGDC, UNAM**

- *Tech Support*.
- Manteinance & Upgrading of equipement (PC & Mac).

> 2007

---

**Small Hardwarestore**

- *Developer*.
- *-- Confidential Company --*.
- Buyer - Supplier - Expenses tracking system
- It was built using JSP technology.

> July, 2006 -- March, 2008.

---

----

> <ivanjpg@ekbt.nl> • +52 55 2840 3943 • Mexico City, México
